import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import * as fs from 'fs'

interface NumberStorage {
  Save(id, number: number): void
  FindById(n: number): number | Error
  FindAll(): number[]
  DeleteAll(): void
}

class NumberRepository implements NumberStorage {
  private path: string

  constructor(path: string) {
    this.path = path
  }

  public Save(id, number: number) {
    fs.writeFileSync(this.path + '/' + id + '.txt', String(number))
  }

  public FindById(n: number): number | Error {
    if (isNaN(n)) {
      return new Error('Invalid ID')
    }
    try {
      const number = fs.readFileSync(this.path + '/' + n + '.txt', { encoding: 'utf8', flag: 'r' })
      return parseInt(number)
    } catch (error) {
      return new Error("ID doesn't exist")
    }
  }

  public FindAll(): number[] {
    let numbers: number[] = []
    if (!fs.existsSync(this.path)) {
      return numbers
    }
    const files = fs.readdirSync(this.path)
    for (const file of files) {
      const number = fs.readFileSync(this.path + '/' + file, { encoding: 'utf8', flag: 'r' })
      numbers.push(parseInt(number))
    }

    return numbers
  }

  public DeleteAll() {
    fs.rmSync(this.path, { recursive: true, force: true })
    fs.mkdirSync(this.path, { recursive: true })
  }
}

export default class RandomNumbersController {
  // MAX_NUMBERS is the maxumin amount of numbers that can be generated
  private MAX_NUMBERS = 1000
  // MAX_RANGE is the largest number that can be generated
  private MAX_RANGE = 1000
  private numberRepository: NumberStorage = new NumberRepository('./data')

  public async generate(ctx: HttpContextContract) {
    const generateSchema = schema.create({
      numberOfSlots: schema.number([rules.range(1, this.MAX_NUMBERS)]),
      max: schema.number([rules.range(1, this.MAX_RANGE)]),
    })

    const params = await ctx.request.validate({ schema: generateSchema })
    this.numberRepository.DeleteAll()
    for (let i = 0; i < params.numberOfSlots; i++) {
      this.numberRepository.Save(i, Math.floor(Math.random() * (params.max + 1)))
    }
    return [
      {
        numberOfSlots: params.numberOfSlots,
        max: params.max,
      },
    ]
  }

  public async getNth(ctx: HttpContextContract) {
    const params = ctx.params
    if (params.n < 1 || params.n > 1001) {
      ctx.response.status(422)
      return [`n must be between 1 and ${this.MAX_NUMBERS + 1}`]
    }
    const r = this.numberRepository.FindById(params.n - 1)
    if (typeof r === 'object') {
      ctx.response.status(404)
      return { error: r.message }
    }
    return { number: r }
  }

  public async getAll(ctx: HttpContextContract) {
    const numbers = this.numberRepository.FindAll()
    let sum = 0
    let min = numbers[0]
    let max = numbers[0]
    for (const number of numbers) {
      if (number < min) {
        min = number
      }
      if (number > max) {
        max = number
      }
      sum += number
    }
    return {
      numbers,
      sum,
      max,
      min,
    }
  }
}
