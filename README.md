# everifin-home-assessment



## How to run
```shell
git clone git@gitlab.com:petertoth1983/everifin-home-assessment.git
cd everifin-home-assessment
cp -R .env.example .env
source .env
npm run dev
```

By default the `3333` port is used.

## Available enpoints
### Generate
Generates `numberOfSlots` number of random numbers between 0 and `max`. After each call it generates new random numbers and the previous numbers will be deleted.

Constrains:
- `numberOfSlots` must be between 1 and 1000
- `max` must be between 1 and 1000

Example request:
```shell
curl --location --request POST 'http://localhost:3333/generate' \
--header 'Content-Type: application/json' \
--data-raw '{
    "numberOfSlots": 100,
    "max":100
}'
```
### Get All
Gets all the generated numbers and the `sum`, `max` and `min` of the numbers.

Example call:
```shell
curl --location --request GET 'http://localhost:3333/getAll' \
```
### Get Nth number
Gets the Nth generated number.

Constrains:
- `n` must be between 1 and 1001

Example call:
```shell
curl --location --request GET 'http://localhost:3333/getNth/1' \
```
